from django.conf import settings
from django.db import models
from django.contrib.auth.models import User


class Product(models.Model):

    name = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='photo/', default="photo/index.jpg")
    description = models.CharField(max_length=500)

    def __unicode__(self):
        return '{0} {1}'.format( self.name, self.description)

# class Rate(models.Model):
#     user = models.ForeignKey(User, related_name='rates_by_user')
#     product = models.ForeignKey(Product)
#     rate = models.IntegerField(default=0)
#
#     def __unicode__(self):
#         return '{0} {1}'.format( self.user, self.product)

class Comment(models.Model):

    user = models.ForeignKey(User)
    product = models.ForeignKey(Product)
    text = models.CharField(max_length=500)
    date = models.DateTimeField(auto_now_add=True)
    rate = models.IntegerField(default=0)

    def __unicode__(self):
        return '{0} {1}'.format( self.text, self.date)




