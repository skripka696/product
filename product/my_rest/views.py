from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from models import Product, Comment
from serializers import ProductSerializer, CommentSerializer, CommentGetSerializer, CommentPostSerializer,CommentSerializer
from rest_framework import viewsets,status
from django.shortcuts import HttpResponse
import json

class ProductViewSet(viewsets.ModelViewSet):
    '''
    Returns a list of comments.
    Edit, delete and add new ones.

    For more details about all comment [see here][com].
    [com]: http://127.0.0.1:8000/api/comment/

    '''

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)



class CommentViewSet(viewsets.ModelViewSet):
    '''
    Returns a list of products.
    Edit, delete and add new ones.

    For more details about all product [see here][ref].
    [ref]: http://127.0.0.1:8000/api/product/

    '''
    queryset = Comment.objects.all()
    serializer_class = CommentPostSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request):
        queryset = Comment.objects.all().order_by('-date')
        serializer = CommentGetSerializer(queryset, many=True)

        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Comment.objects.filter(product=pk).order_by('-date')
        serializer = CommentGetSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        request.data['user'] = request.user.pk
        serializer = CommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class RatesViewSet(viewsets.ModelViewSet):
#     """
# 	Returns a list of  rates.
#
# 	"""
#
#     queryset = Rate.objects.all()
#     serializer_class = SerialPostRates
#
#     def list(self, request):
#         queryset = Rate.objects.all()
#         serializer = SerialRates(queryset, many=True)
#
#         return Response(serializer.data)
#
#     def create(self, request):
#         request.data['user'] = request.user.pk
#         serializer = SerialRates(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def retrieve(self, request, pk=None):
#         queryset = Rate.objects.filter(product=pk)
#         serializer = SerialRates(queryset, many=True)
#         return Response(serializer.data)

def check_login(request):
    if request.user.is_authenticated():
        return HttpResponse(json.dumps({'result': {'logged': True}, 'username': request.user.username, 'userpk': request.user.pk}),
                        content_type="application/json")
    else:
        return HttpResponse(json.dumps({'result': {'logged': False}}),
                        content_type="application/json")