# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('my_rest', '0010_auto_20150727_0640'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='photo',
            field=models.ImageField(default=b'photo/index.jpg', upload_to=b'/media/photo/'),
        ),
    ]
