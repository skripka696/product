# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('my_rest', '0014_rate'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rate',
            name='product',
        ),
        migrations.RemoveField(
            model_name='rate',
            name='user',
        ),
        migrations.AddField(
            model_name='comment',
            name='rate',
            field=models.IntegerField(default=0),
        ),
        migrations.DeleteModel(
            name='Rate',
        ),
    ]
