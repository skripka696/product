# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('my_rest', '0007_auto_20150724_0957'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='photo',
            field=models.ImageField(default=b'photo/index.jpg', upload_to=b'media/photo/'),
        ),
    ]
