from django.contrib.auth.models import User
from django.forms import widgets
from rest_framework import serializers
from models import Product, Comment

class UserSerializer(serializers.ModelSerializer):
    # username = serializers.CharField(max_length=100)
    class Meta:
        model = User
        fields = ['pk', 'username']

class ProductSerializer(serializers.ModelSerializer):
    # name = serializers.CharField(max_length=100)
    # description = serializers.CharField(max_length=500)
    # photo = serializers.ImageField()

    class Meta:
        model = Product
        fields = ['pk', 'name', 'photo', 'description']


class CommentGetSerializer(serializers.ModelSerializer):
    text = serializers.CharField(max_length=500)
    user = UserSerializer(required=False)

    class Meta:
        model = Comment
        fields = ['pk', 'user', 'text', 'date', 'product','rate']
        # depth = 2

class CommentPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ['pk', 'text', 'date', 'product', 'rate']
        # depth = 2

class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ['pk', 'user', 'text', 'date', 'product', 'rate']


#
# class SerialRatesNoUser(serializers.ModelSerializer):
#     class Meta:
#         model = Rate
#         fields = ['pk', 'product', 'rate']