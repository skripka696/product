from django.contrib import admin
from my_rest.models import Product, Comment
from django.contrib.auth.models import User

#
# class ProductAdmin(admin.ModelAdmin):
#     list_display = '__all__'
class CommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'product','date','text' )

class ProductAdmin(admin.ModelAdmin):
    list_display = ('pk','name','description','photo')



admin.site.register(Product, ProductAdmin)
admin.site.register(Comment, CommentAdmin)


