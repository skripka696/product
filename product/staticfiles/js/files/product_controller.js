// Get list of product from rest framework
productApp.factory('ProductService', function($resource){
	return $resource('http://localhost:8000/api/product/?format=json');
});

// List of products
productApp.controller('ProductController',
	function ($scope, ProductService) {
		ProductService.query(function(result) {
			$scope.productList=result;
		});
});