 //Get product detail from rest framework
productApp.factory('ProductIdService', function($resource){
	return $resource('http://localhost:8000/api/product/1/?format=json', {pk:'@pk'});

});

 //product detail
productApp.controller('ProductIdController',
	function ($location, $route, $scope, $http, $routeParams, ProductIdService) {

		ProductIdService.get({pk: $route.current.params.pk}, function (result) {
			$scope.product = result;
		});

});