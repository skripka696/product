var productApp = angular.module('productApp', [
	'ui.router',
	'ngRoute',
	'ngResource',
	'restangular',
//	'pascalprecht.translate',
//	'ngCookies',
//	'ui.bootstrap',
//	'autoActive',
//	'ngAnimate',
//    'ngStorage',
//    'ngSanitize',
//    'ngTouch',
//    'afOAuth2',
]);



productApp.config(function($routeProvider, $locationProvider) {

//	$routeProvider.when('/product_id/:pk/', {
//			templateUrl: '../static/html/product_id.html'
//	});
	$routeProvider.when('/product_id/:pk/', {
			templateUrl: '../static/html/product_id.html'
	});

});


productApp.config(function($httpProvider) {
	$httpProvider.defaults.xsrfCookieName = 'csrftoken';
	$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});