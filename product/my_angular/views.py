from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, ListView
from my_rest.models import Product, Comment
from django.contrib.auth.models import User
from django.views.generic.edit import FormView, View, UpdateView, DeleteView, BaseUpdateView
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from oauth2_provider.models import Application
from django.contrib.auth import login, logout
from django.http import HttpResponse, HttpResponseRedirect
class Pages(TemplateView):
    template_name = 'my_angular/index.html'


    # def get_context_data(self,*args, **kwargs):
    #     context = super(Pages, self).get_context_data(*args, **kwargs)
    #     context.update(NoteForm=NoteForm())
    #     return context

class GetUserId(UpdateView):
    model = User
    # template_name_suffix = 'personal_cabinet'

    template_name = 'my_angular/personal_cabinet.html'
    success_url = '/personal_cabinet/'
    fields = ['first_name','last_name', 'email']

    def get_context_data(self,*args, **kwargs):
        context = super(GetUserId, self).get_context_data(*args, **kwargs)
        user = self.get_object()
        application = Application.objects.get(user=user)
        context.update(client_id=application.client_id)
        context.update(client_secret=application.client_secret)
        return context


class Login(FormView):
    form_class = AuthenticationForm
    success_url = '/'
    template_name = 'my_angular/login.html'

    def form_valid(self,form):
        user = form.get_user()
        login(self.request, user)
        return super(Login, self).form_valid(form)


class LogOut(View):
    success_url = '/login/'
    template_name = 'my_angular/base.html'

    def get(self,request):
        logout(request)
        return HttpResponseRedirect('/')