 //Get product detail from rest framework
productApp.factory('ProductIdService', function($resource){
	return $resource('/api/product/:pk/?format=json', {pk:'@pk'});

});
 //Get comment detail from rest framework
productApp.factory('CommentIdService', function($resource){
	return $resource('/api/comment/:pk/?format=json', {pk:'@pk'});
});

////Get rate
//productApp.factory('RateIdService', function($resource){
//	return $resource('/api/rate/:pk/?format=json', {pk:'@pk'});
//});

 //product detail
productApp.controller('ProductIdController',
	function ($location, $route, $scope, $http, $routeParams, ProductIdService, CommentIdService) {
		$http.get('/api/checklogin').
			success(function(data){
				$scope.log=data.result.logged;
				console.log(data.result.logged);
		});

		ProductIdService.get({pk: $route.current.params.pk}, function (result) {
			$scope.product = result;
		});

		CommentIdService.query({pk: $route.current.params.pk}, function (result) {

			$scope.comment = result;
			$scope.rate = result;

				$('#rates').rating({
              		min: 0,
					max: 5,
					step: 1,
					size: 'xs',
					showClear: false,
					showCaption: false
           });
           $('#rates').rating(result.rate);


		});
//		RateIdService.query({pk: $route.current.params.pk}, function (result) {
//
//			$scope.rate = result;
//
//				$('#rates').rating({
//              		min: 0,
//					max: 5,
//					step: 1,
//					size: 'xs',
//					showClear: false,
//					showCaption: false
//           });
//           $('#rates').rating(result.rate);
//
//
//		});


		$scope.submitComment = function(){
			var data= {description: $scope.selected_description,
		 			   product:$scope.product.pk,
		 			}

		 	$http({
   				method: 'POST',
    			url: '/api/comment/',
    			data: {
    					text: $scope.selected_description,
		 			   product:$scope.product.pk,
		 			   rate: $scope.selected_rate,
		 			},

			}).success(function (response) {
				console.log(response.user);
				console.log(rate.user);
			}).error(function (e) {
				console.log(e);
				if (e.user!=undefined){
						$scope.error = 'You must be logged'
					}

			});
			$scope.selected_description='';
			$scope.selected_rate='';
			$scope.comment.push(
			CommentIdService.query({pk: $route.current.params.pk}, function (result) {
				$scope.comment = result;

				})

			);

		};
});