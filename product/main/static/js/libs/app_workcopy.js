'use strict';

var productsApp = angular.module('productsApp', [
	'ui.router'
	,'ngRoute'
	,'ngResource'
	, 'restangular'
	, 'ngFileUpload'
	, 'pascalprecht.translate'
	, 'ui.bootstrap'
	//, 'ngCookies'
]);


// productsApp.config(function($stateProvider) {
// 	$stateProvider.state('/', { 
// 		url: '/',
// 		templateUrl: 'catalog_templ/index.html',
// 		controller: 'ProductController'
// 	}).state('ProductDetail', { 
// 		url: '/product/:id/',
// 		//templateUrl: 'catalog_templ/productdetail.html',
// 		controller: 'ProductDetailController'
// 	});
// });


var translations_en = {
	HEADLINE: 'Hello there, This is my awesome app!',
	INTRO_TEXT: 'And it has i18n support!',
	ProductName: 'NAME',
	ProductCategory: 'CATEGORY',
	ProductDescription: 'DESCRIPTION',
	ProductPrice: 'PRICE',
	ProductImage: 'IMAGE',
	BUTTON_LANG_EN: 'english',
	BUTTON_LANG_DE: 'german'
};

var translations_de = {
	HEADLINE: 'Hey, das ist meine großartige App!',
	INTRO_TEXT: 'Und sie untersützt mehrere Sprachen!',
	ProductName: 'NAME',
	ProductCategory: 'KATEGORIE',
	ProductDescription: 'BESCHREIBUNG',
	ProductPrice: 'PREIS',
	ProductImage: 'BILD',
	BUTTON_LANG_EN: 'englisch',
	BUTTON_LANG_DE: 'deutsch'
};


productsApp.config(function($routeProvider, $translateProvider, $locationProvider) {
	$routeProvider.when('/product/:pk', {
		templateUrl: 'catalog_templ/productdetail.html',    
		controller: 'ProductDetailController'
	});
	$locationProvider.html5Mode(true);
	//$translateProvider.useCookieStorage()
	$translateProvider.translations('en_EN', translations_en);
	$translateProvider.translations('de_DE', translations_de);
	$translateProvider.preferredLanguage('en_EN');
	$translateProvider.useSanitizeValueStrategy('escaped');
});



productsApp.config(function($httpProvider) {
	$httpProvider.defaults.xsrfCookieName = 'csrftoken';
	$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

productsApp.factory('ProductService', function($resource){
	return $resource('http://localhost:8000/productsapi/?format=json');
});

productsApp.factory('ProductDetailService', function($resource){
	return $resource('http://localhost:8000/productsapi/:pk/?format=json', {pk:'@pk'});
});

productsApp.factory('CategoryService', function($resource){
	return $resource('http://localhost:8000/categoryapi/?format=json');
});


productsApp.controller('Ctrl', function ($scope, $translate) {
  $scope.changeLanguage = function (key) {
	$translate.use(key);
  };
});


productsApp.controller('ProductController', 
	function ($scope, Restangular, ProductService, ProductDetailService, $location) {
		ProductService.query(function(result) {
			$scope.productsList=result;
		});
	$scope.detailpage = function (item) {
		console.log(item.pk);
		var pk=item.pk;
	};
});
productsApp.controller('ProductDetailController',
	function ($location, $route, $scope, $routeParams, ProductDetailService) {
		console.log($location);
		console.log($route);

		$scope.$on('$locationChangeSuccess', function (ev, current, prev) {
			console.log($route)
			ProductDetailService.get({pk: $route.current.params.pk}, function (result) {
				$scope.product_ = result;
			});
		});
});

productsApp.controller('CategoryController', 
	function ($scope, CategoryService) {
		CategoryService.query(function(result) {
			$scope.categoryList=result;
		});
});

productsApp.controller('ProductAddController', function($scope, $http, $q, Upload) {

	$scope.submitJob = function () {

		var f = document.getElementById('id_image_url').files[0], r = new FileReader();
		r.onloadend = function(e){
			var data = e.target.result;
		};
		var fd = new FormData();
		fd.append("file", f);

		$scope.$watch('files', function () {
			$scope.upload($scope.files);
		});
		var image = $scope.image_url;
		image = image.replace("C:\\fakepath\\", "images\\product_image\\");

		$scope.upload = function () {
			Upload.upload({
				//url: '../../main/files/media/images/product_image',
				url: '/productsapi/',
				file: fd,
				fields: {'name': $scope.name, 
						'description': $scope.description, 
						'category':$scope.category, 
						'price':$scope.price,
						'image_url': image,
				},
				progress: function(e){}
			});
		};
		fileUpload.uploadFileToUrl($scope.image_url, '/media/images/product_image/');
		
		// if ($scope.product_add_form.$valid) {
		// 	var d = $q.defer();
		// 	var image = $scope.image_url;
		// 	image = image.replace("C:\\fakepath\\", "images\\product_image\\");
		// 	$http({
		// 		method: 'POST',
		// 		headers: { 'Content-Type': 'application/json;charset=utf-8'},
		// 		url: '/productsapi/',
		// 		data: {name: $scope.name, 
		// 			description: $scope.description, 
		// 			category:$scope.category, 
		// 			price:$scope.price,
		// 			image_url: image
		// 		},
		// 	}).success(function (response) {
		// 		console.log("Okey")
		// 		d.resolve()
		// 	}).error(function (e) {
		// 		d.reject('Server error!');
		// 	});
		// 	return d.promise;
		// } else {
		// 	toastr.error('Fill all required fields');
		// }

		// var fd = new FormData();
		// fd.append('name', $scope.name) ;
		// fd.append(	'description', $scope.description); 
		// fd.append(	'category',$scope.category); 
		// fd.append(	'price',$scope.price);
		// fd.append(	'image_url', $scope.image_url);
		// console.log(fd)
		// $http({
		// 	method: 'POST',
		// 	url: '/productsapi/',
		// 	headers: {'Content-Type': 'multipart/form-data'},
		// 	data: fd,
		// 	transformRequest: function(data, headersGetterFunction) {
		// 		return data; // do nothing! FormData is very good!
		// 	}
		// }).success(function(data, status) {
		// }).error(function(data, status) {
		// });
	};
});
